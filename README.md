# LWC session Basecamp examples

This is a SFDX project that contains examples for Basecamp LWC session. 

1. Create a scratch org from a dev hub which has LWC enabled

	sfdx force:auth:web:login -d
	sfdx force:org:create -d 30 -f config/project-scratch-def.json -s

2. Install Dreamhouse app unlocked package from https://github.com/dreamhouseapp/dreamhouse-sfdx#installing-dreamhouse-using-an-unlocked-package

2. Deploy our code:

	sfdx force:source:push

2. Assign yourself the "Basecamp LWC demo" permission set
	sfdx force:user:permset:assign -n Basecamp_LWC_Demo


